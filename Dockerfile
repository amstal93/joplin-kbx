FROM kalilinux/kali-rolling
ARG KBX_APP_VERSION=1.4.19

RUN apt update && apt install -y wget 
RUN wget https://github.com/laurent22/joplin/releases/download/v$KBX_APP_VERSION/Joplin-$KBX_APP_VERSION.AppImage
RUN chmod 755 Joplin-$KBX_APP_VERSION.AppImage
RUN ./Joplin-$KBX_APP_VERSION.AppImage --appimage-extract

RUN rm Joplin-$KBX_APP_VERSION.AppImage
RUN apt-get remove -y wget && apt-get -y --purge autoremove && apt-get clean
RUN rm -rf /var/lib/apt

RUN chown root squashfs-root/chrome-sandbox
RUN chmod 4755 squashfs-root/chrome-sandbox
RUN mkdir /kaboxer ; echo $KBX_APP_VERSION > /kaboxer/version

CMD /squashfs-root/AppRun
