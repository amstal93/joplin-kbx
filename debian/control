Source: joplin-kbx
Section: utils
Priority: optional
Maintainer: Kali Developers <devel@kali.org>
Uploaders: Sophie Brun <sophie@offensive-security.com>
Build-Depends: debhelper-compat (= 13), kaboxer
Standards-Version: 4.5.1
Homepage: https://github.com/laurent22/joplin
Vcs-Browser: https://gitlab.com/kalilinux/packages/joplin-kbx
Vcs-Git: https://gitlab.com/kalilinux/packages/joplin-kbx.git
Rules-Requires-Root: no

Package: joplin-kbx
Architecture: amd64 i386
Depends: ${misc:Depends}
Description: open source note taking and to-do application
 This package contains a free, open source note taking and to-do application,
 which can handle a large number of notes organised into notebooks. The notes
 are searchable, can be copied, tagged and modified either from the
 applications directly or from your own text editor. The notes are in Markdown
 format.
 .
 Notes exported from Evernote via .enex files can be imported into Joplin,
 including the formatted content (which is converted to Markdown), resources
 (images, attachments, etc.) and complete metadata (geolocation, updated time,
 created time, etc.). Plain Markdown files can also be imported.
 .
 The notes can be synchronised with various cloud services including Nextcloud,
 Dropbox, OneDrive, WebDAV or the file system (for example with a network
 directory). When synchronising the notes, notebooks, tags and other metadata
 are saved to plain text files which can be easily inspected, backed up and
 moved around.
